import 'bootstrap/dist/css/bootstrap.min.css';
import $ from 'jquery';
import Popper from 'popper.js';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import React from 'react';
import ReactDOM from 'react-dom';
//import './index.css';
import Login from './Action/Login';
import Maps from './Action/Maps';
import Nso from './Action/Nso';
import test from './componentnso/Slidebarnso';

import * as serviceWorker from './serviceWorker';
import {Router,Route,Link,browserHistory} from '@version/react-router-v3';
import {createStore} from 'redux';

ReactDOM.render(
    <React.StrictMode>
        <Router history={browserHistory}>
            <Router path={"/"} component={Nso} />
            <Router path={"/map"} component={Nso} />
            <Router path={"map/:name"} component={Nso} />
            <Router path={"/login"} component={Login} />
            <Router path={"/test"} component={test} />

        </Router>
    </React.StrictMode>,
    document.getElementById('root')


  );

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
