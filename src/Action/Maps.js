import React, { Component } from "react";
import ReactDOM from "react-dom";
import 'ol/ol.css';
import Map from 'ol/Map';
import View  from 'ol/View';
import {defaults as defaultControls, ScaleLine} from 'ol/control';
import TileLayer from 'ol/layer/Tile';
import TileWMS from 'ol/source/TileWMS';
import Slidebar from "../componentnso/Slidebarnso";
import Control from "../componentmap/Control";
import {Fill, Stroke, Style, Text} from 'ol/style';
import mapcenter from '../geojson/center.geojson'; // I got a sample geojson from the web



import {
  GeoJSON,
  XYZ
} from 'ol/format'
import {

  Vector as VectorLayer
} from 'ol/layer'
import {
  Vector as VectorSource,
  OSM as OSMSource,
  XYZ as XYZSource,
  TileWMS as TileWMSSource
} from 'ol/source'
import {
  Select as SelectInteraction,
  defaults as DefaultInteractions
} from 'ol/interaction'
import {
  Attribution,
  ZoomSlider,
  Zoom,
  Rotate,
  MousePosition,
  OverviewMap,
  defaults as DefaultControls
} from 'ol/control'

import {
  Projection,
  get as getProjection
} from 'ol/proj'

// End Openlayers imports

class Maps extends Component {
  constructor(props) {
      super(props)
      this.updateDimensions = this.updateDimensions.bind(this)
  }
  updateDimensions(){
      const h = window.innerWidth >= 992 ? window.innerHeight : 400
      this.setState({height: h})
  }
  componentWillMount(){
      window.addEventListener('resize', this.updateDimensions)
      this.updateDimensions()
  }
  componentDidMount(){
    var style = new Style({
      fill: new Fill({
        color: 'rgba(255, 255, 255, 0.6)'
      }),
      stroke: new Stroke({
        color: '#319FD3',
        width: 1
      }),
      text: new Text({
        font: '12px Calibri,sans-serif',
        fill: new Fill({
          color: '#000'
        }),
        stroke: new Stroke({
          color: '#fff',
          width: 3
        })
      })
    });
    var green =  new Style({
      fill: new Fill({
          color: [15, 236, 66, 0.2]
      }),
      stroke: new Stroke({
          color: [15, 236, 66, 9],
          width: 1,
          lineJoin: 'round'
      })
  });

    var Thailand = new VectorLayer({
      source: new VectorSource({
        url: mapcenter,
        format: new GeoJSON()
      }),
      style:green
    });

      // Create an Openlayer Map instance with two tile layers
      const map = new Map({
          //  Display the map in the div with the id of map
          target: 'map',
          layers: [
              new TileLayer({
                  source: new XYZSource({
                      url: 'https://{a-c}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                      projection: 'EPSG:3857'
                  })
              }),
              new TileLayer({
                  source: new TileWMSSource({
                      url: 'https://ahocevar.com/geoserver/wms',
                      params: {
                          layers: 'topp:states',
                          'TILED': true,
                      },
                      projection: 'EPSG:4326'
                  }),
                  name: 'USA'
              }),
              new TileLayer({
                title: "wms",
                source: new TileWMSSource({
                    url: 'http://mapserv.nso.go.th/geoserver/NSOGIS/wms?',
                    params: { 'LAYERS': 'NSOGIS:HALLOWEENLAYER', 'TILED': true, 'SLD': 'http://statgis.nso.go.th/sld/INDPOP_SPK2/POPINCS.sld' },
                    serverType: 'geoserver',
                    attributions: "GISDA @COPPYRIGHT",

                }),
                visible: true,
            }),
          ],
          // Add in the following map controls
          controls: DefaultControls().extend([
              new ZoomSlider(),
              new MousePosition(),
              new ScaleLine(),
              new OverviewMap()
          ]),
          // Render the tile layers in a map view with a Mercator projection
          view: new View({
              projection: 'EPSG:3857',
              center: [11212786.887401765,1700587.5947962767],
              zoom: 5
          })
      })
      //map.addLayer(Thailand);
  }
  componentWillUnmount(){
      window.removeEventListener('resize', this.updateDimensions)
  }
  render(){
      const style = {
          width: '100%',
          height: '80vh',
          backgroundColor: '#cccccc',
      }
      return (
                  <div id='map' style={style} >
                    <Slidebar pnso={this.props.menu}/>
                    <Control/>
                  </div>
      )
  }
}

export default Maps;
