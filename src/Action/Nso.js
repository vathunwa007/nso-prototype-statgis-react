import React, { Component } from "react";
import ReactDOM from "react-dom";
import Navbar from "../componentnso/Navbar";
import Map from "./Maps";
import Tabbar from "../componentnso/Tabbar";
import Footer from "../componentnso/Footer";
import styled from 'styled-components';




class Nso extends Component{
    constructor(props){
        super(props)
        this.state={
            slidepnso: false,
        }

    }
    componentDidMount(){
        if(this.props.params.name != null){
            this.setState({
             slidepnso: true
            });
         }

    }
render(){

    return(
        <div>
            <Navbar name={this.props.params.name}/>
            <Tabbar />
            <Map menu={this.state.slidepnso}/>
            <Footer/>
        </div>
    )
}
}

export default Nso;