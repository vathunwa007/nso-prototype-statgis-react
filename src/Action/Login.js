import React, { Component } from "react";
import ReactDOM from "react-dom";
import logo from '../images/logo.png';

// import ReCAPTCHA from "react-google-recaptcha";

import {
  BrowserRouter,
  Route,
  Switch as RouterSwitch,
  HashRouter,
  browserHistory
} from "react-router-dom";
import { withRouter } from 'react-router-dom';


import axios from "axios";
import Swal from "sweetalert2";

// import Config from '../config/prod';
//import Config from '../config/dev';
const configURL  = 'http://gis.nso.go.th/backend';


const loginTokenWithBearer = window.localStorage.getItem('token');

export default class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            errors: {},
            captcha: false,
            dataStatusReady: false,
            dataModuleStatusReady: false
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.recaptchaLoaded = this.recaptchaLoaded.bind(this);
        this.verifyCallback = this.verifyCallback.bind(this);
        this.checkDataStatus = this.checkDataStatus.bind(this);
    }
    recaptchaLoaded() {
        console.log('captcha successfully loaded');
    }

    handleChange() {
        if (this.state.captcha) {
            console.log('success')
        } else {
            console.log('unsuccess')
        }
    }

    verifyCallback(response) {
        if (response) {
            this.setState({
                captcha: true
            });
        }
    }


    handleSubmit() {
        if (this.validateForm()
            // && this.state.captcha === true
        ) {
            try {
                const user = {
                    USER_NAME: this.state.username,
                    PASSWORD: this.state.password,
                };
                axios({
                    method: 'post',
                    url: configURL + '/api/login',
                    data: user,
                    config: { headers: { 'Content-Type': 'multipart/form-data', } }
                }).then((res) => {
                    if (res.status === 200) {
                        let loginToken = res.data.success.token;
                        window.localStorage.setItem("token", loginToken);
                        Swal.fire({
                            position: 'top',
                            icon: 'success',
                            title: 'Login Success.',
                            showConfirmButton: false,
                            timer: 6000
                        });
                        // loading sidebar and name user
                        axios({
                            method: 'post',
                            url: configURL + '/api/getdata',
                            headers: { 'Authorization': 'Bearer ' + loginToken },
                        }).then(async (response) => {
                            // profile data
                            let profileData = await response.data.Profile_Data;
                            window.localStorage.setItem("prodata", JSON.stringify(profileData));
                            // sidebar check data
                            let permissionData = await response.data.Permission_Data;
                            window.localStorage.setItem("perdata", JSON.stringify(permissionData));
                            this.setState({
                                dataStatusReady: true,
                            });
                            this.checkDataStatus();
                        }).catch((err) => {
                            console.log(err);
                            Swal.fire({
                                position: 'top',
                                icon: 'error',
                                title: 'Error Get Data User.',
                                showConfirmButton: false,
                                timer: 2000
                            });
                        });
                        axios({
                            method: 'post',
                            url: configURL + '/api/module',
                            headers: { 'Authorization': 'Bearer ' + loginToken },
                        }).then(async (response2) => {
                            if (response2.status == 200) {
                                let schemaArrayData = await response2.data;
                                let schemaDefault = 'NSO_STAT_GIS'
                                // schema default
                                window.localStorage.setItem("scmsl", schemaDefault);
                                // schema list
                                window.localStorage.setItem("scmdata", JSON.stringify(schemaArrayData));
                                this.setState({
                                    dataModuleStatusReady: true,
                                });
                                this.checkDataStatus();
                            }
                        }).catch((err) => {
                            console.log(err);
                            Swal.fire({
                                position: 'top',
                                icon: 'error',
                                title: 'Error Get Data Stat.',
                                showConfirmButton: false,
                                timer: 2000
                            });
                        });
                    } else {
                        console.log("res.status not 200");
                    }
                }).catch((err) => {
                    console.log(err);
                    Swal.fire({
                        position: 'top',
                        icon: 'error',
                        title: 'Please check Username and Password again.',
                        showConfirmButton: false,
                        timer: 2000
                    });
                });
            } catch (err) {
                console.error(err);
                Swal.fire({
                    position: 'top',
                    icon: 'error',
                    title: 'System error. Please try again later.',
                    showConfirmButton: false,
                    timer: 2000
                });
            }
        } else {
            Swal.fire({
                position: 'top',
                icon: 'error',
                title: 'Please fulfill all required data.',
                showConfirmButton: false,
                timer: 2000
            });
        }
    }

    checkDataStatus() {
        if (this.state.dataStatusReady && this.state.dataModuleStatusReady) {
            window.location.reload()
        } else {
            Swal.fire({
                title: 'Loading data... Please wait...',
                timer: 30000,
                onBeforeOpen: () => {
                    Swal.showLoading()
                }
            }).then((result) => {
                if (result.dismiss === Swal.DismissReason.timer) {
                    // console.log('I was closed by the timer')
                }
            })
        }
    }

    validateForm() {
        let fields = this.state;
        let errors = {};
        let formIsValid = true;

        if (!fields["username"]) {
            formIsValid = false;
            errors["usernameError"] = "Username is required";
        }

        if (!fields["password"]) {
            formIsValid = false;
            errors["passwordError"] = "Password is required";
        }
        this.setState({
            errors: errors
        });
        return formIsValid;
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="container">
                    <div className="card card-login mx-auto mt-5" style={{marginTop:'50%'}}>
                        <div className="card-header">
                            <img src={logo}style={{width:'350px',margin:'0% 30%'}} className="img-fluid" alt="Responsive image" />
                        </div>
                        <div className="card-body">
                            <form>
                                <div className="form-group">
                                    {/* <div className="form-label-group"> */}
                                        {/* <label htmlFor="inputEmail"> Username or Email address </label> */}
                                        <input
                                            type="text"
                                            id="inputEmail"
                                            className="form-control rounded-pill border-0 shadow-sm px-4"
                                            placeholder="Email address or Username"
                                            required
                                            autoFocus="autofocus"
                                            onChange={(event) => {
                                                this.setState({ username: event.target.value })
                                            }}
                                        />
                                        <div style={{ 'color': 'red', 'fontSize': '12px' }}>{this.state.errors.usernameError}</div>
                                    {/* </div> */}
                                </div>
                                <div className="form-group">
                                    {/* <div className="form-label-group"> */}
                                        {/* <label htmlFor="inputPassword"> Password </label> */}
                                        <input
                                            type="password"
                                            id="inputPassword"
                                            className="form-control rounded-pill border-0 shadow-sm px-4 text-primary"
                                            placeholder="Password"
                                            required="required"
                                            onChange={(event) => this.setState({ password: event.target.value })}
                                        />
                                        <div style={{ 'color': 'red', 'fontSize': '12px' }}>{this.state.errors.passwordError}</div>
                                    {/* </div> */}
                                </div>
                                <div className="form-group container ml-2">
                                    {/* <Recaptcha
                                        sitekey="6LdS4eAUAAAAANNr2fTW9SHfuHi-J38ZYLnMlb5v"
                                        render="explicit"
                                        theme="dark"
                                        onloadCallback={this.recaptchaLoaded}
                                        verifyCallback={this.verifyCallback}
                                    /> */}
                                </div>
                                <div className="custom-control custom-checkbox mb-3">
                                    <input id="customCheck1" type="checkbox" className="custom-control-input"/>
                                    <label htmlFor="customCheck1" className="custom-control-label">Remember password</label>
                                </div>
                                {/* โค้ดเก่า <a className="btn btn-primary btn-block" href="index.php">Login</a> */}
                                <button type="button" className="btn btn-primary btn-block text-uppercase mb-2 rounded-pill shadow-sm"
                                    onClick={() => this.handleSubmit()}>Login</button>
                            </form>
                            <div className="text-center">
                                {/*<a class="d-block small mt-3" href="register.php">Register an Account</a>*/}
                                {/* <a className="d-block small" href="forgot-password.php">Forgot Password?</a> */}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

if (document.getElementById("app")) {
  ReactDOM.render(<Login />, document.getElementById("app"));
}
