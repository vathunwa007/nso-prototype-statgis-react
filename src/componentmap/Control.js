import React, { Component } from 'react'
import image048 from '../icon/image048.png';
import image046 from '../icon/image046.png';
import image047 from '../icon/image047.png';
import image045 from '../icon/image045.png';
import image050 from '../icon/image050.png';
import image051 from '../icon/image051.png';
import image052 from '../icon/image052.png';
import image053 from '../icon/image053.png';
import image054 from '../icon/image054.png';
import image055 from '../icon/image055.png';
import image056 from '../icon/image056.png';
import image057 from '../icon/image057.png';
import imagepoly from '../icon/poly.png';
import "../css/control.css";


export default class Control extends Component {
    render() {
        return (
            <div className="control">
                  <div className="customoption">
        <a id="mouse"><img src={image048} width={25} draggable="true" data-bukket-ext-bukket-draggable="true" /></a>
        <a id="zoom-out"><img src={image046} width={25} draggable="true" data-bukket-ext-bukket-draggable="true" /></a>
        <a id="zoom-in"><img src={image047} width={25} draggable="true" data-bukket-ext-bukket-draggable="true" /></a>
        <a id="fullscreen" onclick="centerpolygon('narathiwat')"><img src={image045} width={25} draggable="true" data-bukket-ext-bukket-draggable="true" /></a>
      </div>
      <div className="menuoption">
        <a data-toggle="tooltip" title="ข้อมูลที่น่าสนใจ"><img src={image050} alt="" draggable="true" data-bukket-ext-bukket-draggable="true" /></a>
        <div className="dropdown">
          <a className data-toggle="tooltip" id="SwipeMap" title="Swipe Map"><img src={image051} width={30} draggable="true" data-bukket-ext-bukket-draggable="true" /></a>
        </div>
        <div className="dropdown">
          <a data-toggle="tooltip" id="slidemap" title="Slide Map"><img src={image052} width={30} draggable="true" data-bukket-ext-bukket-draggable="true" /></a>
          <div className="dropdown-content" style={{width: '150px'}}>
          </div>
        </div>
        <div className="dropdown">
          <a data-toggle="tooltip" title="ค้นหาพื้นที่"><img src={imagepoly} width={30} draggable="true" data-bukket-ext-bukket-draggable="true" /></a>
          <div className="dropdown-content" style={{width: '150px'}}>
            <select id="selectfilter">
              <option value={1}>Polygon</option>
              <option value={2}>Polyline</option>
              <option value={3}>Line</option>
              <option value={4}>Point</option>
            </select>
          </div>
        </div>
        <div className="dropdown">
          <a data-toggle="tooltip" title="วาดภาพบนแผนที่"><img src={image057} width={30} draggable="true" data-bukket-ext-bukket-draggable="true" /></a>
          <div className="dropdown-content " style={{width: '450px'}}>
            <div className="input-group">
              <input type="number" aria-label="First name" className="form-control" placeholder={150} />
              <div className="input-group-prepend">
                <span className="input-group-text">ถึง</span>
              </div>
              <input type="number" aria-label="Last name" className="form-control" placeholder={350} />
            </div>
          </div>
        </div>
        <div className="dropdown">
          <a data-toggle="tooltip" title="ช่วงชั่นข้อมูล"><img src={image053} width={30} draggable="true" data-bukket-ext-bukket-draggable="true" /></a>
          <div className="dropdown-content" style={{width: '150px'}}>
          </div>
        </div>
        <div className="dropdown">
          <a data-toggle="tooltip" title="ชั้นข้อมูลแผนที่"><img src={image056} width={30} draggable="true" data-bukket-ext-bukket-draggable="true" /></a>
          <div className="dropdown-content" style={{width: '200px'}}>
            <input type="checkbox" name="layer" id="q1" defaultValue="mapstandard" /><label htmlFor="q1">แบบ Manaul</label>
            <br />
            <input type="checkbox" name="layer" id="q2" defaultValue="OSMterrain" /><label htmlFor="q2">แบบ Natural Breaks</label>
            <br />
            <input type="checkbox" name="layer" id="q3" defaultValue="OSMtoner" /><label htmlFor="q3">แบบ Equal Interval</label>
            <br />
            <input type="checkbox" name="layer" id="q4" defaultValue="Thaichote" /><label htmlFor="q4">แบบ Quantile</label>
            <br />
            <input type="checkbox" name="layer" id="q5" defaultValue="Thaichote" /><label htmlFor="q5">แบบ Standard Deviation</label>
            <br />
          </div>
        </div>
        <div className="dropdown">
          <a data-toggle="tooltip" title="รูปแบบแผนที่สถิติ"><img src={image055} width={30} draggable="true" data-bukket-ext-bukket-draggable="true" /></a>
          <div className="dropdown-content" style={{width: '300px', top: '-350px'}}>
            <p className="font-weight-bold">แผนที่สถิติ</p>
            <input type="checkbox" name="layer" id="Choropleth" defaultValue="mapstandard" /><label htmlFor="Choropleth">Choropleth map</label>
            <br />
            <input type="checkbox" name="layer" id="Barchart" defaultValue="OSMterrain" /><label htmlFor="Barchart">Barchart map</label>
            <br />
            <input type="checkbox" name="layer" id="checkpie" defaultValue="OSMtoner" /><label htmlFor="checkpie">Pie Chart</label>
            <br />
            <input type="checkbox" name="layer" id="Proportion" defaultValue="Thaichote" /><label htmlFor="Proportion">Proportion Circle</label>
            <br />
            <p className="font-weight-bold">ชั้นข้อมูลแผนที่อื่นๆ</p>
            <p className="ml-3 font-weight-bold">เขตการปกครอง</p>
            <input type="checkbox" name="layer" id="layer" defaultValue="OSMwatercolor" /><label htmlFor="layer">ภาค</label>
            <br />
            <input type="checkbox" name="layer" id="layer" defaultValue="OSMterrain" /><label htmlFor="layer">จังหวัด</label>
            <br />
            <input type="checkbox" name="layer" id="layer" defaultValue="OSMtoner" /><label htmlFor="layer">อำเภอ</label>
            <br />
            <input type="checkbox" name="layer" id="layer" defaultValue="Thaichote" /><label htmlFor="layer">ตำบล</label>
            <br />
            <input type="checkbox" name="layer" id="layer" defaultValue="mapstandard" /><label htmlFor="layer">เทศบาลและ อบต.</label>
            <br />
            <input type="checkbox" name="layer" id="layer" defaultValue="OSMterrain" /><label htmlFor="layer">หมู่บ้าน</label>
            <br />
            <p className="font-weight-bold">ด้านสังคม</p>
            <input type="checkbox" name="layer" id="layer" defaultValue="OSMterrain" /><label htmlFor="layer">ศูนย์ICTชุมชน</label>
            <br />
            <input type="checkbox" name="layer" id="layer" defaultValue="OSMtoner" /><label htmlFor="layer">สถานศึกษาทั่วประเทศ</label>
            <br />
            <p className="font-weight-bold">ด้านเศรษฐกิจ</p>
            <p className="font-weight-bold">ด้านทรัพยาการธรรมชาติ และสิ่งแวดล้อม</p>
            <input type="checkbox" name id /><label htmlFor="layer">พื้นที่น้ำท่วม</label><br />
            <button className="btn btn-primary btn-block">เพิ่ม WMS</button>
          </div>
        </div>
        <div className="dropdown">
          <a data-toggle="tooltip" title="แผนที่พื้นหลัง"><img src={image054} width={30} draggable="true" data-bukket-ext-bukket-draggable="true" /></a>
          <div className="dropdown-content" style={{width: '150px', top: '-350px'}}>
            <p className="font-weight-bold">แผนที่แบบเปิด</p>
            <input type="radio" name="swipemap" id={0} defaultValue="mapstandard" /><label htmlFor={0}>Open Street Map</label>
            <p className="font-weight-bold">แผนที่แบบปิด</p>
            <p className="font-weight-bold">Bing Map</p>
            <input type="radio" name="swipemap" id={1} defaultValue="bing3" /><label htmlFor={1}>เส้นถนน</label>
            <br />
            <input type="radio" name="swipemap" id={2} defaultValue="bing0" /><label htmlFor={2}>แผนที่ภูมิประเทศ</label>
            <br />
            <input type="radio" name="swipemap" id={3} defaultValue="bing1" /><label htmlFor={3}>แผนที่ดาวเทียม</label>
            <br />
            <input type="radio" name="swipemap" id={4} defaultValue="bing2" /><label htmlFor={4}>แผนที่ผสม</label>
            <br />
            <p className="font-weight-bold">Google Map</p>
            <input type="radio" name="swipemap" id={5} defaultValue="Google Road Names" /><label htmlFor={5}>เส้นถนน</label>
            <br />
            <input type="radio" name="swipemap" id={6} defaultValue="Google Road Map" /><label htmlFor={6}>แผนที่ภูมิประเทศ</label>
            <br />
            <input type="radio" name="swipemap" id={7} defaultValue="Google Satellite" /><label htmlFor={7}>แผนที่ดาวเทียม</label>
            <br />
            <input type="radio" name="swipemap" id={8} defaultValue="Google Satellite & Roads" /><label htmlFor={8}>แผนที่ผสม</label>
            <br />
            <p className="font-weight-bold">แผนที่อื่นๆ</p>
            <input type="radio" name="swipemap" id={9} defaultValue="OpenstreetMaptoner" /><label htmlFor={9}>พื้นหลังสีขาว</label>
            <br />
            <input type="radio" name="swipemap" id={10} defaultValue="OpenMapdark" /><label htmlFor={10}>พื้นหลังสีดำ</label>
            <br />
            <input type="radio" name="swipemap" id={11} defaultValue="OpenstreetMapwatercolor" /><label htmlFor={11}>พื้นน้ำ</label>
            <br />
            <input type="radio" name="swipemap" id={12} defaultValue="Thaichote" /><label htmlFor={12}>ดาวเทียมไทยโชติ</label>
            <br />
          </div>
        </div>
      </div>
            </div>
        )
    }
}
