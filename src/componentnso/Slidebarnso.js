import React, { Component } from "react";
import "../css/slidebar.css";
import styled from "styled-components";
import ReactDOM from "react-dom";

const Button = styled.button`
  cursor: pointer;
  background: #51b6bb;
  width: 20%;
  height: 65px;
  display: inline-block;
  color: #fff;
  padding-bottom: 40px;
  border: none;
  &:hover {
    background-color: whitesmoke;
    color: black;
  }
  &.active {
    color: black;
    background-color: white;
  }
`;
const MenuButton = styled.button`
  cursor: pointer;
  height: 45px;
  width: 40%;
  display: inline-block;
  border: none;
  color: #fff;
  background: #23898e;
  padding-bottom: 40px;
  &:hover {
    background-color: whitesmoke;
    color: black;
    border-bottom: 1px solid white;
  }
  &.active {
    color: black;
    background-color: white;
    border-bottom: 5px solid white;
  }
`;
const Btopen = styled.button`
  position: absolute;
  z-index: 9;
  /*background:url(https://image.flaticon.com/icons/svg/617/617819.svg) no-repeat;*/
  background-color: #23898e;
  cursor: pointer;
  width: 35px;
  height: 70px;
  border: none;
  top: 50%;
  left: 418px;
  transition: 1s;
  i {
    transition: 1s;
  }
  &:hover {
    i {
      margin-right: 10px;
      transform: rotate(180deg);
    }
  }
`;

class Slidebarnso extends Component {
  constructor(props) {
    super(props);
    this.test = this.test.bind(this);

    this.state = {
      menu1: true,
      menu2: false,
      menu3: false,
      btmenu1: true,
      btmenu2: false,
      btmenu3: false,
      btmenu4: false,
      btmenu5: false
    };
  }
  closemenu = () => {
    this.setState({
      menu1: false,
      menu2: false,
      menu3: false,
      btmenu1: false,
      btmenu2: false,
      btmenu3: false,
      btmenu4: false,
      btmenu5: false
    });
  };
  closebutton = () => {
    this.setState({
      btmenu1: false,
      btmenu2: false,
      btmenu3: false,
      btmenu4: false,
      btmenu5: false
    });
  };
  test(name){
    console.log(name);

  }
  render() {
    return (
      <div>
        <div className="sidenav">
          <Btopen className="">
            <i className="fa fa-chevron-right fa-2x text-light"></i>
          </Btopen>
          {this.props.pnso && (
            <div
              className="tab d-flex justify-content-around "
              style={{ border: "0px solid #155356" }}
            >
              <MenuButton
                className={`menubutton ${this.state.menu1 ? "active" : ""}`}
                onClick={() => {
                  this.closemenu();
                  this.setState({ menu1: true });
                }}
              >
                ข้อมูลเฉพาะของจังหวัด
              </MenuButton>
              <MenuButton
                className={`menubutton ${this.state.menu2 ? "active" : ""}`}
                onClick={() => {
                  this.closemenu();
                  this.setState({ menu2: true });
                }}
              >
                ข้อมูลเฉพาะของกลุ่มจังหวัด
              </MenuButton>
              <MenuButton
                className={`menubutton ${this.state.menu3 ? "active" : ""}`}
                onClick={() => {
                  this.closemenu();
                  this.setState({ menu3: true });
                }}
              >
                ข้อมูลที่ใช้ร่วมกัน
              </MenuButton>
            </div>
          )}
          <div className="tab d-flex justify-content-around">
            <Button
              className={`${this.state.btmenu1 ? "active" : ""}`}
              onClick={() => {
                this.closebutton();
                this.setState({ btmenu1: true });
              }}
            >
              รายการข้อมูล
            </Button>
            <Button
              className={`${this.state.btmenu2 ? "active" : ""}`}
              onClick={() => {
                this.closebutton();
                this.setState({ btmenu2: true });
              }}
            >
              รายการยอดนิยม
            </Button>
            <Button
              className={`${this.state.btmenu3 ? "active" : ""}`}
              onClick={() => {
                this.closebutton();
                this.setState({ btmenu3: true });
              }}
            >
              รายการตามหน่วยงาน
            </Button>
            <Button
              className={`${this.state.btmenu4 ? "active" : ""}`}
              onClick={() => {
                this.closebutton();
                this.setState({ btmenu4: true });
              }}
            >
              รายการตามประเภทค่าสถิติ
            </Button>
            <Button
              className={`${this.state.btmenu5 ? "active" : ""}`}
              onClick={() => {
                this.closebutton();
                this.setState({ btmenu5: true });
              }}
            >
              ค้นหารายการ
            </Button>
          </div>
          <div className="sidecontent">
            <p>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quasi
              velit, ratione atque iusto nisi quisquam dolorum veniam vero quis!
              Necessitatibus culpa similique adipisci atque! Voluptates minus
              atque veritatis tempore magni?
            </p>
          </div>
        </div>
      </div>
    );
  }
}

export default Slidebarnso;
