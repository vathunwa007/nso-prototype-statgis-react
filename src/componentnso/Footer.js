import React, { Component } from "react";
import imgfooter from '../icon/image148.png';

class Footer extends Component{
    render() {
        const style = {
            position: 'relative',
            left: 0,
            bottom: 0,
            width: '100%',
            height:'60px',
            color:'white',
            backgroundColor: '#558ED5',
        }
      return (
        <div className="footer" style={style}>
        <strong>สงวนลิขสิทธิ์@ พ.ศ.2563 กองนโยบายและวิชาการสถิติ สำนักงานสถิติแห่งชาติ</strong>
        <br /><a style={{color:'white'}} href="http://">นโยบายการรักษาความมั่นคงปลอดภัย</a>|<a style={{color:'white'}} href="http://">นโยบายการคุ้มครองข้อมูลส่วนบุคคล</a>
        <img src={imgfooter} alt="" style={{float: 'right', height: '30px'}} />
      </div>
      );
    }
  }

  export default Footer;