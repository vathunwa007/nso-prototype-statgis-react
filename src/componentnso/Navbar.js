import React, { Component } from "react";
import {Link} from '@version/react-router-v3';
import 'bootstrap/dist/css/bootstrap.min.css';
import "../css/home.css";
import logo from '../images/logo.png';
import imguser from '../icon/image005.png';
import imgmap1 from '../icon/image022.png';
import imgmap2 from '../icon/image019.png';
import imgmap3 from '../icon/image020.png';
import imgmap4 from '../icon/image021.png';


class Navbar extends Component {
  constructor(props){
    super(props)

  }
    render(){

      return (
        <div >
        <nav className="navbar navbar-expand-lg navbar-light bg-light" style={{height:'80px'}}>
          <a className="navbar-brand mr-1" href=""><img src={logo} className="img-fluid top-logo" alt="Responsive image" style={{width:'250px'}} /></a>
          <span className="navbar-text text-primary">
            ระบบนำเสนอแผนที่สถิติ
      <h3 className="text-primary">สำนักงานสถิติแห่งชาติ{this.props.name ? "("+this.props.name+")" :"(ส่วนกลาง)"}</h3>
          </span>
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon" />
          </button>
          <div className="collapse navbar-collapse " id="navbarTogglerDemo02">
            <ul className="nav navbar-nav ml-auto">
              <li className="nav-item">
                <a className="nav-link active" href="#"><strong>หน้าหลัก</strong></a>
              </li>
              <li className="nav-item">
                <Link to="/suduku" className="nav-link" href="#"><strong>วิธีการใช้งาน</strong></Link>
              </li>
              <li className="nav-item">
                <Link to="/login" className="nav-link" href="#"><strong>NSO-GISPOTAL</strong></Link>
              </li>
            </ul>
            <ul className="navbar-nav ml-auto ml-md-0 mr-n4">
              <li className="nav-item dropdown no-arrow">
                <a className="nav-link dropdown-toggle " href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <span className="badge badge-danger" style={{fontSize: '10px'}}>3</span>
                  <i className="fas fa-user-circle fa-3x text-info mt-2 hide" />
                  <img src={imguser} alt="" width={50} />
                </a>
                <div className="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                  <a className="dropdown-item" href="#">Settings</a>
                  <a className="dropdown-item" href="#">Activity Log</a>
                  <div className="dropdown-divider" />
                  <a className="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">Logout</a>
                </div>
              </li>
            </ul>
            <button className="btn btn-link btn-sm text-dark order-1 order-sm-0 hide" id="sidebarToggle" href="#">
              <i className="fas fa-bars fa-3x" />
            </button>
          </div>
        </nav>
        {/* Logout Modal*/}
        <div className="modal fade" id="logoutModal" tabIndex={-1} role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button className="close" type="button" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div className="modal-body">Select "Logout" below if you are ready to end your current session.</div>
              <div className="modal-footer">
                <button className="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a className="btn btn-primary" href="login.php">Logout</a>
              </div>
            </div>
          </div>
        </div>
        <nav className="navbar navbar-light breadcrumb " style={{height: '50px', backgroundColor: '#558ED5',filter:'drop-shadow(0 1px 4px rgba(0, 0, 0, 0.2))'}}>
          <form className="form-inline ">
            <li className="breadcrumb-item">
              <a href="#" className="text-light">หน้าหลัก</a>
            </li>
            <li className="breadcrumb-item active text-light">ระบบนำเสนอแผนที่สถิติ</li>
            <li className="breadcrumb-item active text-light">สำนักงานสถิติแห่งชาติ</li>
            <li className="text-light">&nbsp;|&nbsp;</li>
            <li className="breadcrumb-item active text-light">รายการข้อมูลสถิติที่เลือก</li>
            <li className="breadcrumb-item active text-light">จำนวนเกษตรในเขตปฎิรูปที่ดิน</li>
          </form>
          <div className="form-inline" id="menu-top">
            <button id="maptab1" type="button" name="map" className="maptab active"><img src={imgmap1} alt="" title="แผนที่" width={35} onclick="openslideright('maptab1', 'slide-off')" /></button>
            <button id="maptab2" type="button" name="mapchart" className="maptab"><img src={imgmap2} alt="" title="กราฟ" width={35} onclick="openslideright('maptab2', 'slide-chart')" /></button>
            <button id="maptab3" type="button" name="maptable" className="maptab"><img src={imgmap3} alt="" title="ตาราง" width={35} onclick="openslideright('maptab3', 'slide-table')" /></button>
            <button id="maptab4" type="button" name="mapdetail" className="maptab"><img src={imgmap4} alt="" title="คำอธิบาย" width={35} onclick="openslideright('maptab4', 'slide-detail')" /></button>
          </div>
        </nav>
      </div>
      );
    }
  }

  export default Navbar;
