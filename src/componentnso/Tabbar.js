import React, { Component } from "react";
import "../css/toolbar.css";

class Tabbar extends Component{
    render() {
      return (
          <div id="toolbal" className="d-flex flex-row d-flex justify-content-around" >
            <div className="dropdown">
              <button className="" type="button"><strong>เวลา</strong> </button>
              <div className="dropdown-content t3">
                <div className="row">
                  <p className="col-6 text-center"><strong>ปี พ.ศ. :</strong></p>
                  <p className="col-6"><select id="selectfilter">
                      <option value={1}>2563</option>
                      <option value={2}>2562</option>
                      <option value={3}>2561</option>
                      <option value={4}>2560</option>
                    </select></p>
                  <p className="col-6 text-center"><strong>คาบเวลา:</strong></p>
                  <p className="col-6"><select id="selectfilter">
                      <option value={1}>รายไตรมาส</option>
                      <option value={2}>รายไตรมาส</option>
                      <option value={3}>รายไตรมาส</option>
                      <option value={4}>รายไตรมาส</option>
                    </select></p>
                  <p className="col-6 text-center"><strong>ไตรมาสที่:</strong></p>
                  <p className="col-6"><select id="selectfilter">
                      <option value={1}>1</option>
                      <option value={2}>2</option>
                      <option value={3}>3</option>
                      <option value={4}>4</option>
                    </select></p>
                </div>
                <button  style={{width: '100%'}}>ตกลง</button>
              </div>
            </div>
            <div className="dropdown">
              <button className="" type="button"><strong>พื้นที่</strong> </button>
              <div className="dropdown-content t1">
                <div className="row">
                  <p className="col-6 text-center"><strong>ระดับ :</strong></p>
                  <p className="col-6"><select id="selectfilter">
                      <option value={1}>ภาค</option>
                      <option value={2}>จังหวัด</option>
                      <option value={3}>อำเภอ</option>
                      <option value={4}>ตำบล</option>
                      <option value={4}>เทศบาล</option>
                      <option value={4}>หมู่บ้าน</option>
                      <option value={4}>อบต.</option>
                      <option value={4}>ชุมชน</option>
                    </select></p>
                </div>
                <button className style={{width: '100%'}}>ตกลง</button>
              </div>
            </div>
            <div className="dropdown">
              <button className="" type="button"><strong>การจัดจำแนก</strong> </button>
              <div className="dropdown-content t3">
                <div className="row">
                  <p className="col-6 "><input type="checkbox" name id /><strong>เพศ:</strong></p>
                  <p className="col-6">ชาย</p>
                  <p className="col-6 "><input type="radio" name id /><strong>ช่วงอายุแบบที่ 1:</strong></p>
                  <p className="col-6">5-15 ปี</p>
                  <p className="col-6 "><input type="radio" name id /><strong>ช่วงอายุแบบที่ 2:</strong></p>
                  <p className="col-6">15-59 ปี</p>
                  <p className="col-6 "><input type="checkbox" /><strong>การศึกษา :</strong></p>
                  <p className="col-6">ประถมศึกษา</p>
                  <p className="col-6 "><input type="checkbox" /><strong>อาชีพ :</strong></p>
                </div>
                <button className style={{width: '100%'}}>ตกลง</button>
              </div>
            </div>
            <div className="dropdown">
              <button className="" type="button"><strong>แหล่งข้อมูล</strong> </button>
              <div className="dropdown-content t3">
                <div className="row">
                  <p className="col-4 text-center"><strong>กรม :</strong></p>
                  <p className="col-8"><select id="selectfilter">
                      <option value={1}>สำนักงานสถิติแห่งชาติ</option>
                      <option value={2}>สำนักงานเศรษฐกิจการเกษตร</option>
                    </select></p>
                </div>
                <button className style={{width: '100%'}}>ตกลง</button>
              </div>
            </div>
            <div className="dropdown" style={{borderRight: '2px solid white'}}>
              <button className="" type="button"><strong>อธิบายข้อมูล</strong> </button>
              <div className="dropdown-content t5">
                <div className="row">
                  <p className="col-6 "><strong>หน่วยงานเจ้าของข้อมูล :</strong></p>
                  <p className="col-6">สำนักงานเศรษฐกิจการเกษตร</p>
                  <p className="col-6 "><strong>ความถี่ในการจัดเก็บ:</strong></p>
                  <p className="col-6">รายปี</p>
                  <p className="col-6 "><strong>ช่วงเวลาข้อมูล:</strong></p>
                  <p className="col-6">2555-2559</p>
                  <p className="col-6 "><strong>วิธีการได้มา:</strong></p>
                  <p className="col-6">สำรวจ</p>
                  <p className="col-6 "><strong>แหล่งที่มา:</strong></p>
                  <p className="col-6">ศูนย์สารสนเทศยุทธศาสตร์ภาครัฐ</p>
                  <p className="col-6 "><strong>การนำไปใช้ประโยชน์:</strong></p>
                  <p className="col-6">xxxx</p>
                  <p className="col-6 "><strong>คุณภาพข้อมูล:</strong></p>
                  <p className="col-6">XXXXX</p>
                  <p className="col-6 "><strong>สถิติทางการ:</strong></p>
                  <p className="col-6">สาขาเกษตรและการประมง</p>
                  <p className="col-6 "><strong>หน่วยนับ:</strong></p>
                  <p className="col-6">จำนวน</p>
                  <p className="col-6 "><strong>แหล่งข้อมูล:</strong></p>
                  <p className="col-6">http://google.com</p>
                  <p className="col-6 "><strong>หมายเหตุ:</strong></p>
                  <p className="col-6" />
                </div>
                <button className style={{width: '100%'}}>ตกลง</button>
              </div>
            </div>
            <div className="dropdown">
              <button className=""type="button"><strong>องค์ความรู้</strong> </button>
              <div className="dropdown-content t7">
                <div className="row">
                  <div className="col-12">
                    <img src="icon/image270.png" alt="" width={25} /><span>ลิงค์เว็บไซต์ที่เข้าถึงบทความหรือสิ่งพิมพ์ที่ใช้ข้อมูลสถิติตามรายการที่เลือกในการนำเสนอข้อมูลเพื่อเป็นองค์ความรู้ในการใช้ประโยชน์จากข้อมูลสถิติ</span>
                  </div>
                  <p className="col-12 text-primary">รายการเว็บไซต์(ไม่มี)</p>
                  <p className="col-4 text-center"><strong>ลำดับ</strong></p>
                  <p className="col-4 text-center"><strong>หัวข้อ</strong></p>
                  <p className="col-4 text-center"><strong>เว็บลิงค์</strong></p>
                  <p className="col-4 text-center">1</p>
                  <p className="col-4">การช่วยเหลือเกษตรไทย</p><a href="http://www.nso.go.th">www.nso.go.th</a>
                </div>
              </div>
            </div>
            <div className="dropdown">
              <button className=" " type="button"><strong>ระบบสารสนเทศที่เกี่ยวข้อง</strong> </button>
              <div className="dropdown-content t7">
                <div className="row">
                  <div className="col-12">
                    <img src="icon/image270.png" alt="" width={25} /><span>ลิงค์เว็บไซต์เข้าถึงระบบสารสนเทศที่เกี่ยวข้องกับรายการข้อมูลสถิติที่เลือก</span>
                  </div>
                  <p className="col-12 text-primary">รายการเว็บไซต์(ไม่มี)</p>
                  <p className="col-4 text-center"><strong>ลำดับ</strong></p>
                  <p className="col-4 text-center"><strong>หัวข้อ</strong></p>
                  <p className="col-4 text-center"><strong>เว็บลิงค์</strong></p>
                  <p className="col-4 text-center">1</p>
                  <p className="col-4">ระบบเกษตรกร 4.0</p><a href="http://www.nso.go.th">www.nso.go.th</a>
                </div>
              </div>
            </div>
          </div>
      );
    }
  }

  export default Tabbar;