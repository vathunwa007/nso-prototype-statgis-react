import React, { Component } from "react";
import ReactDOM from "react-dom";
import logo from "./logo.svg";
import "../src/css/suduku.css";
import Board from "./componentsuduku/Board";


class Suduku extends Component {
  render() {
    return (
      <div className="App">
        <Board />
      </div>
    );
  }
}

export default Suduku;
